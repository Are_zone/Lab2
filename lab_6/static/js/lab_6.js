var themes = [
        {"id":0,"text":"Red","bcgColor":"#F44336","fontColor":"#FAFAFA"},
        {"id":1,"text":"Pink","bcgColor":"#E91E63","fontColor":"#FAFAFA"},
        {"id":2,"text":"Purple","bcgColor":"#9C27B0","fontColor":"#FAFAFA"},
        {"id":3,"text":"Indigo","bcgColor":"#3F51B5","fontColor":"#FAFAFA"},
        {"id":4,"text":"Blue","bcgColor":"#2196F3","fontColor":"#212121"},
        {"id":5,"text":"Teal","bcgColor":"#009688","fontColor":"#212121"},
        {"id":6,"text":"Lime","bcgColor":"#CDDC39","fontColor":"#212121"},
        {"id":7,"text":"Yellow","bcgColor":"#FFEB3B","fontColor":"#212121"},
        {"id":8,"text":"Amber","bcgColor":"#FFC107","fontColor":"#212121"},
        {"id":9,"text":"Orange","bcgColor":"#FF5722","fontColor":"#212121"},
        {"id":10,"text":"Brown","bcgColor":"#795548","fontColor":"#FAFAFA"}
    ];

localStorage.setItem('themes', JSON.stringify(themes));
var retrievedObject = localStorage.getItem('themes');
var retrievedTheme = localStorage.getItem('newSelected');


$(document).ready(function() {
	//console.log(retrievedObject);
	// Chatting
	$('textarea').keypress(function(m) {
		if (m.which == 13) {
			var msg = $('textArea').val();
			var old = $('.msg-insert').html();
			if (msg.length == 1){
				alert("Message kosong!");
				$('textarea').val("");
			}
			else{
				$('.msg-insert').html(old + '<p class="msg-send">' + msg + '</p>')
				$('textarea').val("");
			}
		}
	});
	// Toggle for arrow
	$('#arrow').click(function() {
		$('.chat-body').toggle();
	});

});


// Calculator
var print = document.getElementById('print');
var erase = false;
var go = function(x) {
  if (x === 'ac') {
    /* implemetnasi clear all */
    print.value = '';
  } else if (x === 'eval') {
      print.value = Math.round(evil(print.value) * 10000) / 10000;
      erase = true;
  } else {
    print.value += x;
  }
};

function evil(fn) {
  return new Function('return ' + fn)();
}

$(document).ready(function() {
	// change color
    $('#selector').select2({
        'data' : JSON.parse(retrievedObject)
    });
    var retrievedTheme = localStorage.getItem('newSelected');
    
	var appliedTheme = JSON.parse(retrievedTheme);
	
	localStorage.setItem('newSelected', JSON.stringify(selectedTheme));
    
});

// Change Theme
$('.apply-button').on('click', function(){  // sesuaikan class button
    // [TODO] ambil value dari elemen select .my-select
    var id = $(".my-select").select2("val");
    data = $.parseJSON(retrievedObject);
    $.each(data, function(i, item) {
    	 // [TODO] cocokan ID theme yang dipilih dengan daftar theme yang ada
	    if(id == item.id){
	    	 // [TODO] ambil object theme yang dipilih
	        var text = item.text;
	        var background = item.bcgColor;
	        var font = item.fontColor;
	        // [TODO] aplikasikan perubahan ke seluruh elemen HTML yang perlu diubah warnanya
	        $('body').css("background-color",background);
	        $('body').css("color", font);
	        // [TODO] simpan object theme tadi ke local storage selectedTheme
	       	var tes = {"selected":{"bcgColor":background,"fontColor":font, "text":text}};
	        localStorage.setItem('newSelected', JSON.stringify(tes));
	    }
    });
});


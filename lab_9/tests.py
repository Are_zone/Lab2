from django.test import TestCase
from django.test import Client
from django.urls import resolve
from .api_enterkomputer import get_drones, get_soundcards, get_opticals
from .custom_auth import auth_login, auth_logout
from .csui_helper import get_access_token

import environ

root = environ.Path(__file__) - 3 # three folder back (/a/b/c/ - 3 = /)
env = environ.Env(DEBUG=(bool, False),)
environ.Env.read_env('.env')

# Create your tests here.
class Lab9UnitTest(TestCase):
        def test_lab_9_url_is_exist(self):
                response = Client().get('/lab-9/')
                self.assertEqual(response.status_code, 200)

	
